//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Reports.rc
//
#define IDD_FORMVIEW                    300
#define IDR_TOOLBAR1                    23007
#define IDC_TFRXPREVIEWX1               23008
#define IDR_TOOLBAR2                    23009
#define ID_TBTN_PRINTOUT                32771
#define ID_TBTN_CREATEPDF               32772
#define ID_TBTN_WHOLEPAGE               32773
#define ID_TBTN_HALFPAGE                32774
#define ID_BTN_CANCEL                   32775
#define ID_TBTN_SENDMAIL                32776
#define ID_BTN_CANCEL2                  33777
#define ID_TBTN_PRINTOUT2               33778
#define ID_TBTN_CREATEPDF2              33779
#define ID_TBTN_SENDMAIL2               33780
#define ID_TBTN_WHOLEPAGE2              33781
#define ID_TBTN_HALFPAGE2               33782
#define ID_TBTN_GROUP_VIEW2             33784

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        23012
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         23012
#define _APS_NEXT_SYMED_VALUE           23000
#endif
#endif
