#include "StdAfx.h"
#include "ReportFrameDoc.h"
#include "ReportFormView.h"
#include "ResLangFileReader.h"
#include ".\reportframedoc.h"


/////////////////////////////////////////////////////////////////////////////
// CMDIReportDoc

IMPLEMENT_DYNCREATE(CMDIReportDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIReportDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIReportDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIReportDoc construction/destruction

CMDIReportDoc::CMDIReportDoc()
{
	// TODO: add one-time construction code here
}

CMDIReportDoc::~CMDIReportDoc()
{
}

BOOL CMDIReportDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMDIReportDoc serialization

void CMDIReportDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIReportDoc diagnostics

#ifdef _DEBUG
void CMDIReportDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIReportDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CTBBtn

BEGIN_MESSAGE_MAP(CTBBtn, CButton)
	//{{AFX_MSG_MAP(CButtonOptions)
	ON_CONTROL_REFLECT(BN_CLICKED, OnTBtnClick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CTBBtn::Create(CWnd *pParent,int id)
{
	m_nBtnID = id;
	return CButton::Create(_T(""),WS_CHILD|WS_VISIBLE|WS_TABSTOP|BS_PUSHBUTTON,CRect(0,0,0,0),pParent,id);
}


void CTBBtn::OnTBtnClick()
{
	// Get a pointer to the CommandBar.
	CXTPCommandBar* pCommandBar = DYNAMIC_DOWNCAST(CXTPCommandBar, GetParent());
	if (pCommandBar)
	{
		// Get the ID of the currently selected button on the Options toolbar.
		int nID = ::GetDlgCtrlID(m_hWnd);

		// Loop through all of the controls on the Options CommnadBar.
		for (int i = 0; i < pCommandBar->GetControls()->GetCount(); i++)
		{
			// Use the DYNAMIC_DOWNCAST macro to cast the CButtonOptions control to a CXTPControlCustom control.
			CXTPControlCustom* pControl = DYNAMIC_DOWNCAST(CXTPControlCustom, pCommandBar->GetControl(i));
			if (pControl)
			{
				// Set the control's vertical draw style while the toolbar is docked vertically.
				::SendMessageA(pCommandBar->GetParent()->GetSafeHwnd(),WM_COMMAND,m_nBtnID,0);
			}
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CMDIReportFrame

IMPLEMENT_DYNCREATE(CMDIReportFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIReportFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIReportFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SYSCOMMAND()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_MESSAGE(ID_REPORT_MSG, OnMessageFromReport)

	ON_XTP_CREATECONTROL()

	ON_COMMAND(ID_BTN_CANCEL,OnTBtnCancelClick)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIReportFrame construction/destruction

CMDIReportFrame::CMDIReportFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
}

CMDIReportFrame::~CMDIReportFrame()
{
}

void CMDIReportFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_REPORT_FRAME_KEY);
	SavePlacement(this, csBuf);

	m_bFirstOpen = TRUE;
}

void CMDIReportFrame::OnClose()
{
	CMDIChildWnd::OnClose();
}

BOOL CMDIReportFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
//	cs.style &= ~(FWS_ADDTOTITLE | FWS_PREFIXTITLE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return CMDIChildWnd::PreCreateWindow(cs);
}


/////////////////////////////////////////////////////////////////////////////
// CMDIReportFrame diagnostics

#ifdef _DEBUG
void CMDIReportFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIReportFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CMDIReportFrame message handlers

int CMDIReportFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;
	CString sToolTip3;
	CString sToolTip4;
	CString csToolTipMail;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	if (!m_wndStatusBar.Create(this) ||
			!m_wndStatusBar.SetIndicators(indicators,sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0(_T("Failed to create status bar\n"));
		return -1;      // fail to create
	}

	if (!m_TBtn.Create(this, ID_BTN_CANCEL) )
	{
		TRACE0(_T("Failed to create Toolbar button\n"));
		return -1;      // fail to create
	}

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			m_sPage	=  xml->str(IDS_STRING100);
			m_sCancel	= xml->str(IDS_STRING101);
			m_sClose	= xml->str(IDS_STRING102);
			m_sTBtnCaption	= m_sClose;

			sToolTip1 = xml->str(IDS_STRING32771);
			sToolTip2 = xml->str(IDS_STRING32772);
			sToolTip3 = xml->str(IDS_STRING32773);
			sToolTip4 = xml->str(IDS_STRING32774);
			csToolTipMail = xml->str(IDS_STRING32775);
		}
		delete xml;
	}


	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);

	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR1)
				{		
					hResModule = LoadLibraryEx((sTBResFN), NULL, DONT_RESOLVE_DLL_REFERENCES|LOAD_LIBRARY_AS_DATAFILE);
					if (hResModule)
					{
						pCtrl = p->GetAt(0);
						pCtrl->SetTooltip(sToolTip1);
						hIcon = LoadIcon(hResModule, (RSTR_TB_PRINT));
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(1);
						pCtrl->SetTooltip(sToolTip2);
						hIcon = LoadIcon(hResModule, (RSTR_TB_PDF));
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(2);
						pCtrl->SetTooltip(csToolTipMail);
						hIcon = LoadIcon(hResModule, _T("MAIL"));
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(3);
						pCtrl->SetTooltip(sToolTip3);
						hIcon = LoadIcon(hResModule, (RSTR_TB_HALF_PAGE));
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(4);
						pCtrl->SetTooltip(sToolTip4);
						hIcon = LoadIcon(hResModule, (RSTR_TB_WHOLE_PAGE));
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						FreeLibrary(hResModule);
					}	// if (hResModule)

				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))

	m_bOkToClose = TRUE;
	m_bFirstOpen = TRUE;
	

	return 0;
}

int CMDIReportFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_BTN_CANCEL)
	{
		if (m_TBtn.GetSafeHwnd() != NULL)
		{
			m_TBtn.MoveWindow(0,0,55,24);
			m_TBtn.SetOwner(&m_wndToolBar);

			CXTPControlCustom * pCB = CXTPControlCustom::CreateControlCustom(&m_TBtn);
			pCB->SetID(ID_BTN_CANCEL);
			pCB->SetFlags(xtpFlagManualUpdate);
			lpCreateControl->buttonStyle = xtpButtonAutomatic;
			lpCreateControl->pControl = pCB;
		}
		return TRUE;
	}

	return TRUE;
}

void CMDIReportFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE && m_bOkToClose)
		CMDIChildWnd::OnSysCommand(nID,lParam);
	
	if ((nID & 0xFFF0) == SC_MAXIMIZE || 
			(nID & 0xFFF0) == SC_MINIMIZE || 
			(nID & 0xFFF0) == SC_MOVE || 
			(nID & 0xFFF0) == SC_RESTORE || 
			(nID & 0xFFF0) == SC_SIZE)
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

void CMDIReportFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
	{
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
	}
}

void CMDIReportFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_REPORT_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIReportFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIReportFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CMDIReportFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);

}


LRESULT CMDIReportFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CString sPath;
	_user_msg *msg = (_user_msg *)lParam;
	CDocument* pDocument = GetActiveDocument();

	POSITION pos = pDocument->GetFirstViewPosition();
	if (pos)
	{
		CReportFormView *pReport = (CReportFormView *)pDocument->GetNextView(pos);
		if (pReport)
		{
			sPath.Format(_T("%s\\%s"),getReportsDir(),msg->getName());
			pReport->setReportData((sPath),(msg->getFileName()),(msg->getName()),_T(""));
//			pReport->showReport(_T(sPath),_T(msg->getFileName()),_T(msg->getName()));
		}	// if (pReport)
	}	// if (pos)

	return 0L;
}

LRESULT CMDIReportFrame::OnMessageFromReport( WPARAM wParam, LPARAM lParam )
{

	switch (wParam)
	{
		case ID_REPORT_ONPROGRESS_MSG:
		{
			CString S;
			S.Format(_T("%s : %d"),m_sPage,(int)lParam);
			m_wndStatusBar.SetPaneText(0,S);
			break;
		}
		case ID_REPORT_ONPROGRESS_START_MSG:
		{
			m_TBtn.SetWindowText(m_sCancel);
			m_bOkToClose = FALSE;
			break;
		}
		case ID_REPORT_ONPROGRESS_STOP_MSG:
		{
			m_TBtn.SetWindowText(m_sClose);
			m_bOkToClose = TRUE;
			break;
		}
	};
	return 0L;
}

void CMDIReportFrame::OnTBtnCancelClick(void)
{
	CDocument* pDocument = GetActiveDocument();

	POSITION pos = pDocument->GetFirstViewPosition();
	if (pos)
	{
		CReportFormView *pReport = (CReportFormView *)pDocument->GetNextView(pos);
		if (pReport)
		{
			if (!m_bOkToClose)
			{
				pReport->cancelReport();
				m_bOkToClose = TRUE;
			}
			else
			{
				PostMessage(WM_COMMAND, ID_FILE_CLOSE);
			}
		}	// if (pReport)
	}	// if (pos)
}
