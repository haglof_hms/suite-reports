#include "StdAfx.h"
#include "FastReportParser.h"

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}

//////////////////////////////////////////////////////////////////////////////////
// FRParser

FRParser::FRParser(void)
{
	CHECK(CoInitialize(NULL));

}

FRParser::~FRParser()
{
  CoUninitialize();
}

// Methods for Loagins and Saving xml file(s); 060407 p�d

BOOL FRParser::load(LPCTSTR file)
{
	_variant_t varLoadResult((bool)FALSE);
  try
  {
		
		// Create MSXML2 DOM Document
		CHECK(pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0"));

		// Set parser property settings
		pDomDoc->async = VARIANT_FALSE;


		// Validate during parsing
		pDomDoc->validateOnParse = VARIANT_TRUE;

    // Load the sample XML file
    varLoadResult = pDomDoc->load(file);
  }
  catch(...)
  {
		AfxMessageBox(_T("Error on XMLHandler::Start()"));
  }
	return varLoadResult;
}

// Public

// Read attribute ReportOptions.Name in root tag <TfrxReport>
BOOL FRParser::getReportOptions_Name(LPTSTR data)
{
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNodePtr pAttrChild2 = NULL;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(_bstr_t(FR_ROOT_NODE));

	if (pNode)
	{	
		pNode->get_attributes( &attributes1 );
		MSXML2::IXMLDOMNodePtr pAttrChild1 = attributes1->getNamedItem(_bstr_t(FR_REPORTOPTIONS_NAME));

		strcpy((char *)data,pAttrChild1->text);

		AfxMessageBox(data);
		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}
