#ifndef _FASTREPORTPARSER_H_
#define _FASTREPORTPARSER_H_

// this class reads attributes in a FastReports report (xml); 061020 p�d
class FRParser
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;

public:
	FRParser(void);

	virtual ~FRParser();

	BOOL load(LPCTSTR);

	BOOL getReportOptions_Name(LPTSTR);
};



#endif