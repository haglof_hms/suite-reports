#include "frxpreview.h"
#include "ReportFrameDoc.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>

using namespace std;



#if _MSC_VER < 1300
// Use this for MS Visual Studio 6.
#import "path\\to\\FastReport3.dll" named_guids auto_rename
#else
// This code preffered for MS Visual Studio.NET
#import "libid:d3c6fb9b-9edf-48f3-9a02-6d8320eaa9f5" named_guids auto_rename
#endif
using namespace FastReport;
/*
#if _MSC_VER < 1300

	//////////////////////////////////////////////////////////////////////////
	//
	// Use this for MS Visual Studio 6
	//
	//////////////////////////////////////////////////////////////////////////
	#import "..\\..\\..\\bin\\FastReport3.dll" named_guids
//	#import "..\\..\\..\\frxCOM\\frxCOM.dll" named_guids

#else

	//////////////////////////////////////////////////////////////////////////
	//
	// This code preffered for MS Visual Studio.NET
	//
	//////////////////////////////////////////////////////////////////////////
	#import "libid:d3c6fb9b-9edf-48f3-9a02-6d8320eaa9f5" named_guids

#endif

using namespace FastReport;
*/



typedef enum { 
							 CONNECTION_TYPE_NONE,
							 CONNECTION_TYPE_SQL_SERVER,
							 CONNECTION_TYPE_MYSQL_SERVER,
							 CONNECTION_TYPE_MYSQL_LOCAL
							} enumCONNECTION_TYPES;


class CReportFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CReportFormView)

protected:
	CReportFormView();           // protected constructor used by dynamic creation
	virtual ~CReportFormView();

	CfrxPreview		m_frxPreview;

public:
	IfrxReportPtr	m_pReport;
protected:

	CString m_sReportFN;
	CString m_sDocTitle;
	CString m_sTextFileName;
	CString m_sArguments;
  	
	TCHAR m_szDBServerPath[MAX_PATH];
	TCHAR m_szDBUser[32];
	TCHAR m_szDBPsw[32];
	TCHAR m_szDBDSNName[32];
	TCHAR m_szDBLocation[32];
	TCHAR m_szDBName[32];
	SAClient_t m_saClient;

	CString m_sConnectionString;

	BOOL m_bIsUMDatabase;
	BOOL m_bIsConnected;

	enumAction m_action;

public:
	void showReport(void);

	void setReportData(LPCTSTR fn,LPCTSTR text_file,LPCTSTR doc_title,LPCTSTR args)
	{
		m_sReportFN = fn;
		m_sDocTitle = doc_title;
		m_sTextFileName = text_file;
		m_sArguments = args;
		m_action = DB_FILE;
	}
	void setReportData(LPCTSTR cap,_user_msg& msg,enumAction action)
	{
		m_sDocTitle = cap;
		m_sReportFN = msg.getName();
		m_sTextFileName = msg.getFileName();
		m_sArguments = msg.getArgStr();
		m_action = action;
	}
	
	void cancelReport(void)
	{
		m_pReport->Terminated = TRUE;
	}

public:
	enum { IDD = IDD_FORMVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	CMDIReportDoc* GetDocument();
protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPropertiesView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CReportFormView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnShowWindow(BOOL,UINT);

	afx_msg void OnReportPrint();
	afx_msg void OnReportPDF();
	afx_msg void OnZoomWholePage();
	afx_msg void OnZoomPageWidth();
	afx_msg void OnSendEmail();

	afx_msg LRESULT OnMessageTBComboBox( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


#ifndef _DEBUG  // debug version in MDIFlickDemoView.cpp
inline CMDIReportDoc* CReportFormView::GetDocument()
   { return (CMDIReportDoc*)m_pDocument; }
#endif


