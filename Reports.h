
#ifndef _REPORTS_H_
#define _REPORTS_H_


#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

#include <vector>
/*
// This struct holds index and name of a loaded form; 051212 p�d
typedef struct _index_table
{
	int nTableIndex;
	TCHAR szLanguageFN[MAX_PATH];
	TCHAR szSuite[MAX_PATH];
	BOOL bOneInstance;

	_index_table(int n,LPCTSTR suite,LPCTSTR lang_fn,BOOL one_inst)
	{
		nTableIndex = n;
		strcpy(szLanguageFN,lang_fn);
		strcpy(szSuite,suite);
		bOneInstance = one_inst;
	}
} INDEX_TABLE;


typedef std::vector<INDEX_TABLE> vecINDEX_TABLE;
*/

// Initialize the DLL, register the classes etc
extern "C" void DLL_BUILD InitSuite(CStringArray *,vecINDEX_TABLE &,vecINFO_TABLE &);

// Open a document view
extern "C" void DLL_BUILD OpenSuite(int idx,LPCTSTR func,CWnd *,vecINDEX_TABLE &,int *ret);
extern "C" void DLL_BUILD OpenSuiteEx(_user_msg *msg,CWnd *,vecINDEX_TABLE &,int *ret);
extern "C" void DLL_BUILD OpenSuiteEx2(_user_msg *msg,CWnd *,vecINDEX_TABLE &,int *ret);

#endif