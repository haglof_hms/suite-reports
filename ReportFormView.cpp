// ReportFormView.cpp : implementation file
//

#include "stdafx.h"
#include "Reports.h"
#include "ReportFormView.h"
#include "ReportFrameDoc.h"

#include "Resource.h"
#include ".\reportformview.h"

#define _ATL_ATTRIBUTES

#include <atlbase.h>
CComModule _Module;
#include <atlcom.h>

#include <mapi.h>
#include "IMapi.h"


class CfrxReportEvents : public CComObjectRoot,public IDispatchImpl<IfrxReportEvents,&__uuidof(IfrxReportEvents),&LIBID_FastReport>
{
	BEGIN_COM_MAP(CfrxReportEvents)
		COM_INTERFACE_ENTRY(IDispatch)
		COM_INTERFACE_ENTRY(IfrxReportEvents)
	END_COM_MAP()
	HWND hParent;
public:
		void setParent(HWND h)
		{
			hParent = h;
		}

    STDMETHODIMP raw_OnBeforePrint(struct IfrxComponent * Sender ) { return S_OK; }
    STDMETHODIMP raw_OnAfterPrint(struct IfrxComponent * Sender ) { return S_OK; }
    STDMETHODIMP raw_OnUserFunction(BSTR MethodName,VARIANT Params,VARIANT * ResultValue ) { return S_OK; }
		STDMETHODIMP raw_OnRunDialogs(IfrxPage * Sender ) { return S_OK; }

		STDMETHODIMP raw_OnClickObject(struct	IfrxView * View,long	Button) { return S_OK; }
    STDMETHODIMP raw_OnBeginDoc(struct IfrxComponent * Sender ) { return S_OK; }
    STDMETHODIMP raw_OnEndDoc(struct IfrxComponent * Sender ) { return S_OK; }
    STDMETHODIMP raw_OnPrintReport(struct IfrxComponent * Sender ) { return S_OK; }
    STDMETHODIMP raw_OnProgress(struct IfrxReport * Sender,long ProgressType,int Progress ) 	
		{ 
			if (hParent)
			{
				::SendMessageA(hParent,ID_REPORT_MSG,ID_REPORT_ONPROGRESS_MSG,(LPARAM)Progress);
			}
			return S_OK; //MessageBox(NULL, S, "Demo", MB_OK); 
		}
    
		STDMETHODIMP raw_OnProgressStart() 
		{ 
			if (hParent)
			{
				::SendMessageA(hParent,ID_REPORT_MSG,ID_REPORT_ONPROGRESS_START_MSG,0);
			}
			return S_OK; //MessageBox(NULL, S, "Demo", MB_OK); 
		}
    STDMETHODIMP raw_OnProgressStop()
		{ 
			if (hParent)
			{
				::SendMessageA(hParent,ID_REPORT_MSG,ID_REPORT_ONPROGRESS_STOP_MSG,0);
			}
			return S_OK; //MessageBox(NULL, S, "Demo", MB_OK); 
		}

		STDMETHODIMP raw_OnAfterPrintReport(struct IfrxComponent * Sender ) { return S_OK; }
		STDMETHODIMP raw_OnBeforeConnect(struct IfrxComponent * Sender ) { return S_OK; }

#ifndef STUDIO_3
    STDMETHODIMP raw_OnBeforeConnect(struct IfrxADODatabase * DataBase, VARIANT_BOOL Connected ) { return S_OK; }
#endif
};


int SplitString(const CString& input,const CString& delimiter, CStringArray& results)
{
  int iPos = 0;
  int newPos = -1;
  int sizeS2 = delimiter.GetLength();
  int isize = input.GetLength();

  CArray<INT, int> positions;

  newPos = input.Find (delimiter, 0);

  if( newPos < 0 ) { return 0; }

  int numFound = 0;

  while( newPos >= iPos )
  {
    numFound++;
    positions.Add(newPos);
    iPos = newPos;
    newPos = input.Find (delimiter, iPos+sizeS2); //+1);
  }

  for( int i = 0; i < positions.GetSize(); i++ )
  {
    CString s;
    if( i == 0 )
      s = input.Mid( i, positions[i] );
    else
    {
      int offset = positions[i-1] + sizeS2;
      if( offset < isize )
      {
        if( i == positions.GetSize() )
          s = input.Mid(offset);
        else if( i > 0 )
          s = input.Mid( positions[i-1] + sizeS2, 
                 positions[i] - positions[i-1] - sizeS2 );
      }
    }
    if( s.GetLength() >= 0 )
      results.Add(s);
  }
  return numFound;
}

//const char strDataSource[] = "Provider=MSDASQL.1;Persist Security Info=False;Data Source=Test";
//const char strDataSource[] = "Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ID=sa;Initial Catalog=HMS_Test;Data Source=ODEN;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=PADDATOR;Use Encryption for Data=False;Tag with column collation when possible=False";
//const char strDataSource[] = "Provider=MSDASQL.1;Persist Security Info=False;Data Source=EvalueStand;Extended Properties=DSN=EvalueStand;DBQ=E:\Hagl�fMamagmentSystem\Execute\data.mdb;DriverId=25;FIL=MS Access;MaxBufferSize=2048;PageTimeout=5;";


LPCTSTR szConnectionStringArray[] = 
// SQL Server
//{{"Provider=SQLOLEDB;Password=%s;Persist Security Info=True;User ID=%s;Data Source=%s;Use Encryption for Data=False;Tag with column collation when possible=False;Initial Catalog=%s;Auto Translate=True"},

// MSSQL Windows Authentication; added 081024 Peter
{{_T("Provider=SQLOLEDB;Server=%s;Database=%s;Trusted_Connection=yes;")},

// MSSQL Server Authentication
{_T("Provider=SQLOLEDB;Password=%s;User ID=%s;Data Source=%s;Initial Catalog=%s")},

// MYSQL Server through ODBC
{_T("Provider=MSDASQL.1;Password=%s;Persist Security Info=False;User ID=%s;Data Source=%s;DATABASE=%s")}};

BOOL SetConnection(IfrxReportPtr report,
									 bstr_t connection_str,
									 bstr_t connection_name)
{
  IfrxComponent  * comp1;
  IfrxComponent  * comp2;
  IfrxADODatabase * conn;

	try
	{
		if (report->QueryInterface(__uuidof(IfrxComponent),(PVOID*) &comp1) != S_OK)
			return FALSE;

		comp2 = comp1->FindObject(connection_name);
		
		if (comp2->QueryInterface(__uuidof(IfrxADODatabase),(PVOID*) &conn) != S_OK)
			return FALSE;

		conn->LoginPrompt = false;
		conn->Connected = false;
		conn->ConnectionString = connection_str;
		conn->Connected = true;

		conn->Release();
		comp2->Release();
		comp1->Release();
		return TRUE;
	}
	catch(...)
	{
//		AfxMessageBox("Error open database connection");
		AfxMessageBox(connection_str);
	}
	return FALSE;

}

BOOL GetQueryByName(IfrxReportPtr  report,
										IfrxADOQuery  *query,   
										bstr_t query_name,
										bstr_t query_string)
{
  IfrxComponent  * comp1;
  IfrxComponent  * comp2;

	if (report->QueryInterface(__uuidof(IfrxComponent),(PVOID*) &comp1) != S_OK)
		return FALSE;
  
	comp2 = comp1->FindObject(query_name);

  if (comp2->QueryInterface(__uuidof(IfrxADOQuery),(PVOID*) &query) != S_OK)
		return FALSE;
	
	query->Query = query_string;

  comp2->Release();
  comp1->Release();

	return TRUE;

}

// CReportFormView

IMPLEMENT_DYNCREATE(CReportFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CReportFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_ERASEBKGND()
	ON_WM_SETFOCUS()
	ON_WM_SHOWWINDOW()

	ON_COMMAND(ID_TBTN_PRINTOUT, OnReportPrint)
	ON_COMMAND(ID_TBTN_CREATEPDF, OnReportPDF)
	ON_COMMAND(ID_TBTN_WHOLEPAGE, OnZoomWholePage)
	ON_COMMAND(ID_TBTN_HALFPAGE, OnZoomPageWidth)
	ON_COMMAND(ID_TBTN_SENDMAIL, OnSendEmail)

	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

CReportFormView::CReportFormView()
	: CXTResizeFormView(CReportFormView::IDD)
{
	m_bIsConnected = FALSE;
	m_action = NO_ACTION;
}

CReportFormView::~CReportFormView()
{
}

void CReportFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CActiveX_DemoDlg)
	DDX_Control(pDX, IDC_TFRXPREVIEWX1, m_frxPreview);
	//}}AFX_DATA_MAP

}

void CReportFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	CoInitialize(NULL);

	//  FastReport here.
	//	Create report engine for using with Viewer
	{
		IfrxReportPtr	pReport(__uuidof(TfrxReport));
		m_pReport = pReport;
	}
	

	//  FastReport here.
	//	Link report engine to Viewer. Following code is mandatory for Viewer
	m_frxPreview.SetReport(m_pReport);

	// Get information on Database server used; 081024 Peter
	int auth = 0; // Use Windows Authentication (0 = Yes)
	getDBUserInfo(m_szDBServerPath,m_szDBName,m_szDBUser,m_szDBPsw,&m_saClient,&auth);
	getDBUserInfo(m_szDBServerPath,m_szDBUser,m_szDBPsw,m_szDBDSNName,m_szDBLocation,m_szDBName,&m_saClient);

	m_bIsUMDatabase = TRUE;
	// Setup connectionstring for Datbase; 081024 Peter
	if (m_saClient == SA_SQLServer_Client)
	{
		if( auth == 0 )
		{
			m_sConnectionString.Format(szConnectionStringArray[0],
																	m_szDBLocation,
																	m_szDBName);
		}
		else
		{
			m_sConnectionString.Format(szConnectionStringArray[1],
																	m_szDBPsw,
																	m_szDBUser,
																	m_szDBLocation,
																	m_szDBName);
		}
	}
	else if  (m_saClient == SA_MySQL_Client)
	{
		m_sConnectionString.Format(szConnectionStringArray[2],
															 m_szDBPsw,
															 m_szDBUser,
															 m_szDBDSNName,
															 m_szDBName);
	}
	else
	{
		m_bIsUMDatabase = FALSE;
	}

}

void CReportFormView::OnShowWindow(BOOL bShow,UINT nStatus)
{
//	showReport();
	CXTResizeFormView::OnShowWindow(bShow,nStatus);
}

void CReportFormView::OnDestroy()
{
	CoUninitialize();
}

void CReportFormView::OnClose()
{
	m_pReport->Terminated  = TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CMDIFlickDemoView drawing

void CReportFormView::OnDraw(CDC* pDC)
{
//	CMDIReportDoc* pDoc = GetDocument();
//	ASSERT_VALID(pDoc);
}

BOOL CReportFormView::OnEraseBkgnd(CDC* pDC)
{
	pDC;
	return FALSE;
}

void CReportFormView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_PREVIEW_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// CReportFormView diagnostics

#ifdef _DEBUG
void CReportFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CReportFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}

CMDIReportDoc* CReportFormView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMDIReportDoc)));
	return (CMDIReportDoc*)m_pDocument;
}

#endif //_DEBUG

void CReportFormView::showReport(void)
{
	CString S;
	HRESULT hr;
	BOOL bIsOK = FALSE;
	BOOL bIsArguments = FALSE;
	CString sFilePath;
	CString sArgName;
	TCHAR szValue[127];
	CStringArray arguments;

	if (!m_sArguments.IsEmpty())
	{
		// Make sure the last character in argumant string is a semicolon; 070207 p�d
		if (m_sArguments.Right(1) != _T(";"))
		{
			m_sArguments += _T(";");
		}
		// Check out if there is arguments; 070207 p�d
		//	splitInfo(m_sArguments.GetBuffer(),&nItems,&nLen,';');
		bIsArguments = (SplitString(m_sArguments,_T(";"),arguments) > 0);
/*		
		CString S;
		S.Format(_T("arguments %d"),arguments.GetCount());
		AfxMessageBox(S);
*/
	}

	if (fileExists(m_sReportFN))
	{
		m_frxPreview.Lock();

		if (m_pReport->LoadReportFromFile((m_sReportFN.GetBuffer())) == S_OK)
		{
			// set the document title to be the name of the report
/*			if(strlen(m_pReport->ReportOptions->Name) > 0)
			{
				AfxMessageBox(m_pReport->ReportOptions->Name);
//				((CMDIReportDoc*)GetDocument())->SetTitle(m_pReport->ReportOptions->Name);
			}
			else
			{
//				((CMDIReportDoc*)GetDocument())->SetTitle(m_sReportFN);
			}
//			((CMDIReportDoc*)GetDocument())->UpdateAllViews(NULL);
*/

			// Check, if text filename's empty. This implies an already
			// created report, which my hold arguments; 070207 p�d
			if (m_sTextFileName.IsEmpty() && m_action == TEXT_FILE)
			{
				// Setup and add argumants to report; 070207 p�d
				if (bIsArguments)
				{
					for (int i = 0;i < arguments.GetCount();i++)
					{
						sArgName.Format(_T("arg%d"),i+1);
						_stprintf(szValue,_T("'%s'"),arguments.GetAt(i));
						m_pReport->SetVariable(_bstr_t(sArgName), _variant_t(szValue));
					}
				}	// if (bIsArguments)

				bIsOK = TRUE;
			}
			// Check to see if there's a text_file. If so, display it in the 
			// report window, report loaded from fn; 060627 p�d
			else if (fileExists(m_sTextFileName) && m_action == TEXT_FILE)
			{
				sFilePath.Format(_T("'%s'"),m_sTextFileName);		// OBS! Use ' ' for closing of FileName; 060627 p�d
				m_pReport->SetVariable(_bstr_t("FileName"), _variant_t(sFilePath));

				// Setup and add argumants to report; 070207 p�d
				if (bIsArguments)
				{
					for (int i = 0;i < arguments.GetCount();i++)
					{
						sArgName.Format(_T("arg%d"),i+1);
						_stprintf(szValue,_T("'%s'"),arguments.GetAt(i));
						m_pReport->SetVariable(_bstr_t(sArgName), _variant_t(szValue));
					}
				}	// if (bIsArguments)

				bIsOK = TRUE;
			}
			// Open a report; connected to the Database; 060627 p�d
			// I.e, if we are connected to a database; 060801 p�d
			else if (m_bIsUMDatabase && m_action == DB_FILE)
			{
				// We only need to make the connection to the Databaseserver; 060628 p�d

				if (!m_bIsConnected)
				{
					m_bIsConnected = SetConnection(m_pReport,_bstr_t(m_sConnectionString), _bstr_t("ADODatabase1"));
				}

				// Setup and add argumants to report; 070207 p�d
				if (bIsArguments)
				{
					for (int i = 0;i < arguments.GetCount();i++)
					{
						sArgName.Format(_T("arg%d"),i+1);
						_stprintf(szValue,_T("'%s'"),arguments.GetAt(i));
						m_pReport->SetVariable(_bstr_t(sArgName), _variant_t(szValue));
					}
				}	// if (bIsArguments)

				bIsOK = TRUE;
			}
			if (bIsOK)
			{
				CComObject<CfrxReportEvents> * pEventHandler;

				hr = CComObject<CfrxReportEvents>::CreateInstance(&pEventHandler);

				IConnectionPointContainer *pCPC = NULL;
				IConnectionPoint *pCP = NULL;
				DWORD dwCookie;

				// Check that this is a connectable object.				
			  hr = m_pReport->QueryInterface(__uuidof(IConnectionPointContainer), (void**)&pCPC);
				hr = pCPC->FindConnectionPoint(__uuidof(IfrxReportEvents), &pCP);

				CComPtr<IUnknown> pUnk;
				pEventHandler->QueryInterface(IID_IUnknown, (void**)&pUnk);
				hr = pCP->Advise( pUnk, &dwCookie); 

				CMDIReportFrame *pFrame = DYNAMIC_DOWNCAST(CMDIReportFrame,GetParent());
				if (pFrame != NULL)
				{
					pEventHandler->setParent(pFrame->GetSafeHwnd());
				}

				pCP->Release();
				pCPC->Release();
			}	// if (bIsOK)

			m_frxPreview.Unlock();
//			m_frxPreview.SetZoomMode(2);	// show whole page
			m_pReport->ShowReport();

		}	// if (m_pReport->LoadReportFromFile(_T(m_sReportFN.GetBuffer())))
	}	// if (fileExists(m_sReportFN))
}


// PROTECTED

// CReportFormView message handlers
void CReportFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	
	RECT rect;
	GetClientRect(&rect);

	if (m_frxPreview.GetSafeHwnd())
	{
		setResize(&m_frxPreview,0,0,rect.right,rect.bottom);
		m_frxPreview.UpdateWindow();
	}

}

void CReportFormView::OnReportPrint()
{
	m_frxPreview.Print();
}


void CReportFormView::OnReportPDF()
{
	// Need this to enable ComboBox on Toolbar; 060118 p�d
	CFileDialog *dlg = new CFileDialog( 
			FALSE,				// File save dialog
			_T("pdf"),
			_T(""),
			OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|OFN_EXPLORER,
			_T("pdf (*.pdf)|*.pdf|"), 
			NULL 
			);
	if( dlg->DoModal() == IDOK )
	{
		IfrxBuiltinExports *pExp;
		m_pReport->QueryInterface(__uuidof(IfrxBuiltinExports), (void**) &pExp);
		pExp->ExportToPDF(_bstr_t(dlg->GetPathName()), true, false, false, false, _bstr_t(""), _bstr_t("")); // last two args undocumented; UserPassword and OwnerPassword
		pExp->Release();
	}
	delete dlg;
}

void CReportFormView::OnZoomWholePage()
{
	m_frxPreview.SetZoomMode( 2 );
}

void CReportFormView::OnZoomPageWidth()
{
	m_frxPreview.SetZoomMode( 1 );
}

LRESULT CReportFormView::OnMessageTBComboBox( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

// attach current report in a email
#define BUFSIZE 4096

void CReportFormView::OnSendEmail()
{
	// send the report
	CIMapi mail;
	if (mail.Error() == 0) //IMAPI_SUCCESS)
	{

		CString csTo = _T("email_to");
		_variant_t t = m_pReport->GetVariable(_bstr_t(csTo));
		csTo = t.bstrVal;
		if(!csTo.IsEmpty()) mail.To(csTo);

		CString csSub = _T("email_subject");
		t = m_pReport->GetVariable(_bstr_t(csSub));
		csSub = t.bstrVal;
		if(!csSub.IsEmpty()) mail.Subject(csSub);

		CString csText = _T("email_text");
		t = m_pReport->GetVariable(_bstr_t(csText));
		csText = t.bstrVal;
		if(!csText.IsEmpty()) mail.Text(csText);

		DWORD dwBufSize=BUFSIZE;
		TCHAR lpPathBuffer[BUFSIZE];
		GetTempPath(dwBufSize, lpPathBuffer);	// get the temporary path
		CString csTempPath;
	
		CString csAtt = _T("email_attach");
		t = m_pReport->GetVariable(_bstr_t(csAtt));
		csAtt = t.bstrVal;
		if(!csAtt.IsEmpty())
		{
			csTempPath.Format(_T("%s%s"), lpPathBuffer, csAtt);
		}
		else
		{
			if(m_pReport->ReportOptions->GetName().length() != 0)
			{
				csTempPath.Format(_T("%s%s.pdf"), lpPathBuffer, m_pReport->ReportOptions->GetName().Detach());
//				csTempPath.Format(_T("c:\\temp\\%s.pdf"), m_pReport->ReportOptions->GetName().Detach());
			}
			else
			{
				csTempPath.Format(_T("%sreport.pdf"), lpPathBuffer);
			}
		}

		// create a PDF
		m_frxPreview.Lock();
		IfrxBuiltinExportsPtr pExport;
		pExport = m_pReport;
		pExport->ExportToPDF(_bstr_t(csTempPath), true, false, true, false, _bstr_t(""), _bstr_t("")); // last two args undocumented; UserPassword and OwnerPassword
		m_frxPreview.Unlock();

		mail.Attach(csTempPath);

		mail.Send();
		// Just remove the attachment from disk; 081210 p�d
		if (fileExists(csTempPath))
			removeFile(csTempPath);
	}
	else
	{
		// Do something appropriate to the error...
		AfxMessageBox(_T("No email-client installed!"));
	}
}

// scroll the report vertically
BOOL CReportFormView::OnMouseWheel(UINT fFlags, short nDelta, CPoint point)
{
	m_frxPreview.MouseWheelScroll(nDelta, FALSE, FALSE);

	return 1;
//	return CFormView::OnMouseWheel(fFlags, nDelta, point);
}

/*
arge1;asdf;asdfsdaf;sadfasdf;

mail_to:asdf@asdf.com; 

arg1=aaaa;arg2=bbbb; ......
*/

/*
IfrxComponent
*
pComponent;
IfrxComponent
*
pMemoComp;
IfrxMemoView
*
pMemoObj;
IfrxReportPtr
pReport(__uuidof(TfrxReport));
pReport->LoadReportFromFile("somereport.fr3");
// Query base interface
hr = pReport->QueryInterface(__uuidof(IfrxComponent), (PVOID) &pComponent);
// Find object with name �Memo1�
hr = pComponent->FindObject(_bstr_t("Memo1"), & pMemoComp);
// Query memo interface from founded object
hr = pMemoComp->QueryInterface(__uuidof(IfrxMemoView), (PVOID) & pMemoObj);
// Set the memo text
pMemoObj->Text = _bstr_t("This as a memo label");
pMemoObj->Release();
pMemoComp->Release();
pComponent->Release();
*/
