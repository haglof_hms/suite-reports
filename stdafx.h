// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

//#ifndef _AFX_NO_OLE_SUPPORT
//#include <afxole.h>         // MFC OLE classes
//#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
//#endif // _AFX_NO_OLE_SUPPORT
/*
#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT
*/
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

// Xtreeme toolkit
#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

//#define _XTLIB_NOAUTOLINK
#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

#include "pad_hms_miscfunc.h"					// HMSFuncLib header

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

#define	ID_REPORT_MSG							(WM_USER+100)
#define	ID_REPORT_ONPROGRESS_MSG				(WM_USER+101)
#define	ID_REPORT_ONPROGRESS_START_MSG			(WM_USER+102)
#define	ID_REPORT_ONPROGRESS_STOP_MSG			(WM_USER+103)

#define MSG_IN_SUITE				 			(WM_USER + 10)		// This identifer's used to send messages internally

//////////////////////////////////////////////////////////////////////////////
// Strings in languagefile; 060208 p�d

#define IDS_STRING100							100
#define IDS_STRING101							101
#define IDS_STRING102							102
#define IDS_STRING103							103
#define IDS_STRING104							104

#define IDS_STRING32771							32771
#define IDS_STRING32772							32772
#define IDS_STRING32773							32773
#define IDS_STRING32774							32774
#define IDS_STRING32775							32775
#define IDS_STRING32776							32776
#define IDS_STRING32777							32777

const LPCTSTR PROGRAM_NAME			  			= _T("Reports");					// Name of suite/module, used on setting Language filename; 051214 p�d

//////////////////////////////////////////////////////////////////////////////////////////
//	Main resource dll, for toolbar icons; 051219 p�d

const LPCTSTR TOOLBAR_RES_DLL					= _T("HMSToolBarIcons32.dll");		// Resource dll, holds icons for e.g. toolbars; 051208 p�d

/////////////////////////////////////////////////////////////////////////////////////////////////
// Icon names for toolbar; 060612 p�d
const LPCTSTR RSTR_TB_PRINT						= _T("Skrivut");
const LPCTSTR RSTR_TB_PDF						= _T("Pdf");
const LPCTSTR RSTR_TB_MAIL						= _T("Mail");
const LPCTSTR RSTR_TB_HALF_PAGE  				= _T("Halvsida");
const LPCTSTR RSTR_TB_WHOLE_PAGE				= _T("Helsida");
const LPCTSTR RSTR_TB_GROUPS					= _T("Sok");
const LPCTSTR RSTR_TB_EXPORT					= _T("Export");

/////////////////////////////////////////////////////////////////////////////////////////////////
// Define nodes in a FastReport report file (xml); 061020 p�d
#define FR_ROOT_NODE							_T("//TfrxReport")
#define FR_REPORTOPTIONS_NAME					_T("ReportOptions.Name")



typedef enum {NO_ACTION, DB_FILE,TEXT_FILE } enumAction;


const LPCTSTR REG_REPORT_FRAME_KEY				= _T("Reports\\ReportFrame\\Placement");

//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of the Window
const int MIN_X_SIZE							= 500;
const int MIN_Y_SIZE							= 400;

static UINT indicators[] =
{
	ID_SEPARATOR           // status line indicator
};

/////////////////////////////////////////////////////////////////////////////////////////////////
// Miasc. class
class CTBBtn : public CButton
{
	int m_nBtnID;
public:
	CTBBtn(void)	: CButton()	{}

	BOOL Create(CWnd *pParent,int id);
	//{{AFX_MSG(CTBBtn)
	afx_msg void OnTBtnClick();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

/////////////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions
CString getToolBarResourceFN(void);

CView *getFormViewByID(int idd);

void setWindowTextFromViewByID(int idd,LPCTSTR caption);
