// Reports.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "ReportFormView.h"		// FastReports
#include "ReportFrameDoc.h"		// FastReports
#include "Reports.h"
#include "ResLangFileReader.h"
#include "FastReportParser.h"

#include <afxdllx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static CWinApp* pApp = AfxGetApp();
HINSTANCE hInst = NULL;

static AFX_EXTENSION_MODULE ReportsDLL = { NULL, NULL };

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	hInst = hInstance;
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("Reports.DLL Initializing!\n");
	
	
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(ReportsDLL, hInstance))
			return 0;

	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("Reports.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(ReportsDLL);
	}
	return 1;   // ok
}

// Exported DLL initialization is run in context of running application
void DLL_BUILD InitSuite(CStringArray *user_modules,vecINDEX_TABLE &vecIndex,vecINFO_TABLE &vecInfo)
{
	CString sVersion;
	CString sCopyright;
	CString sCompany;
	// create a new CDynLinkLibrary for this app
	new CDynLinkLibrary(ReportsDLL);

	AfxEnableControlContainer();

	CString sModuleFN = getModuleFN(hInst);
	// Setup the language filename
	CString sLangFN;
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
			RUNTIME_CLASS(CMDIReportDoc),
			RUNTIME_CLASS(CMDIReportFrame),
			RUNTIME_CLASS(CReportFormView)));

	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW,sModuleFN,sLangFN,FALSE));

	// Get version information; 060803 p�d

	const LPCTSTR VER_NUMBER						= _T("FileVersion");
	const LPCTSTR VER_COMPANY						= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

	sVersion		= getVersionInfo(hInst,VER_NUMBER);
	sCopyright	= getVersionInfo(hInst,VER_COPYRIGHT);
	sCompany		= getVersionInfo(hInst,VER_COMPANY);
	vecInfo.push_back(INFO_TABLE(-999,1 /* Suite module */,
					 (TCHAR*)sLangFN.GetBuffer(),
					 (TCHAR*)sVersion.GetBuffer(),
					 (TCHAR*)sCopyright.GetBuffer(),
					 (TCHAR*)sCompany.GetBuffer()));
	
	sVersion.ReleaseBuffer();
	sCopyright.ReleaseBuffer();
	sCompany.ReleaseBuffer();
}



// Use this function, when calling from a ShellData tree; 060619 p�d
void DLL_BUILD OpenSuite(int idx,LPCTSTR func,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	CString sDocTitle;
	CDocTemplate *pTemplate;
	CString sFuncStr;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sLangSet;
	CString sCaption;
	CString sCompareCaption;
	CString sReportFN;
	CString sArgs;
	int nTableIndex;
	int nDocCounter;
	int nNumOfReportsOpen;
	int nSeparatorPos;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;

	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		nTableIndex = vecIndex[i].nTableIndex;
		// Get filename including searchpath to THIS SUITE, as set in
		// the table index vector, for suites and module(s); 051213 p�d
		sVecIndexTableModuleFN = vecIndex[i].szSuite;

		if (nTableIndex == idx && 
				sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
			// Get filename including searchpath to THIS SUITE, as set in
			// the table index vector, for suites and module(s); 051213 p�d
			sVecIndexTableModuleFN = vecIndex[i].szSuite;
			// Need to setup the Actual filename here, because we need to 
			// get the Language set in registry, on Openning a View; 051214 p�d
			sLangFN = vecIndex[i].szLanguageFN;

			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)

	if (bFound)
	{
		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);

		RLFReader *xml = new RLFReader();
		if (xml->Load(sLangFN))
		{
			sCaption = xml->str(nTableIndex);
		}
		delete xml;


		// Check if the document or module is in this SUITE; 051213 p�d
		if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
			// First; find out number of Report wiondows already open; 060627 p�d
			POSITION posA = pApp->GetFirstDocTemplatePosition();
			nNumOfReportsOpen = 0;
			while(posA != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(posA);
				pTemplate->GetDocString(sDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				// Need to add a linefeed, infront of the docName.
				// This is because, for some reason, the document title,
				// set in resource, must have a linefeed.
				// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
				sDocName = '\n' + sDocName;

				if (pTemplate && sDocName.Compare(sResStr) == 0)
				{
					POSITION posDOCA = pTemplate->GetFirstDocPosition();
					while (posDOCA != NULL)
					{
						CDocument* pDocumentA = pTemplate->GetNextDoc(posDOCA);
						sCompareCaption = pDocumentA->GetTitle();
						// Compare title of window and count number of matches for
						// a Report window; 060627 p�d
						if (_tcsncmp(sCompareCaption,
												sCaption,
												_tcslen(sCaption)) == 0)
						{
							nNumOfReportsOpen++;
						}	// if (strncmp(sCompareCaption.GetBuffer(),
					}	// while (posDOCA != NULL)
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)

			// Second; Open a new Report window for THIS report; 060627 p�d
			POSITION posB = pApp->GetFirstDocTemplatePosition();
			while(posB != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(posB);
				pTemplate->GetDocString(sDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				// Need to add a linefeed, infront of the docName.
				// This is because, for some reason, the document title,
				// set in resource, must have a linefeed.
				// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
				sDocName = '\n' + sDocName;

				if (pTemplate && sDocName.Compare(sResStr) == 0)
				{
					
					pTemplate->OpenDocumentFile(NULL);
				//AfxMessageBox(sDocName + _T("  ") + sResStr);

					// Find the CDocument for this tamplate, and set title.
					// Title is set in Languagefile; OBS! The nTableIndex
					// matches the string id in the languagefile; 051129 p�d
					POSITION posDOC = pTemplate->GetFirstDocPosition();
					nDocCounter = 1;

					while (posDOC != NULL)
					{
						CMDIReportDoc* pDocument = (CMDIReportDoc*)pTemplate->GetNextDoc(posDOC);

						if (nDocCounter == (nNumOfReportsOpen + 1) || nNumOfReportsOpen == 0)
						{
							// Get language abbrevation set; 061020 p�d
							sLangSet = getLangSet();
							sReportFN = func;
							nSeparatorPos = sReportFN.Find(';');
							if( nSeparatorPos >= 0 )
							{
								// Extract args & filename; 090825 Peter
								sArgs = sReportFN.Right(sReportFN.GetLength() - nSeparatorPos - 1);
								sReportFN = sReportFN.Left(nSeparatorPos);
							}
							sFuncStr.Format(_T("%s%s\\%s"),getReportsDir(),sLangSet,sReportFN);
							if (fileExists(sFuncStr))
							{
								POSITION pos = pDocument->GetFirstViewPosition();
								if (pos)
								{

									// 2008-01-04 p�d
									// We need to check type of Report. This is done here by
									// comparing the nTableIndex (=> ID OF FORMVIEW), to determin'
									// kind of report.
									// IDD_FORMVIEW		= FastReport
									// IDD_FORMVIEW1	= Crystal Reports
									if (nTableIndex == IDD_FORMVIEW)
									{
										CReportFormView *pReport = (CReportFormView *)pDocument->GetNextView(pos);
										if (pReport)
										{
											// Set the caption of the document. Can be a resource string,
											// a string set in the language xml-file etc.
											sDocTitle.Format(_T("%s [%s]"),sCaption,func);
											pDocument->SetTitle(sCaption);
											pReport->setReportData(sFuncStr,_T(""),sDocTitle,sArgs);
											pReport->showReport();

											if(pReport->m_pReport->ReportOptions->GetName().length() != 0)
											{
												sDocTitle.Format(_T("%s [%s]"), sCaption, pReport->m_pReport->ReportOptions->GetName().Detach());
											}
											else
											{
												// Set the caption of the document. Can be a resource string,
												// a string set in the language xml-file etc.
												sDocTitle.Format(_T("%s - [%s]"), sCaption, extractFileName(sFuncStr));
											}
										}	// if (pReport)

										pDocument->SetTitle(sDocTitle);
									}	// if (nTableIndex == IDD_FORMVIEW)
								}	// if (pos)

							}	// if (fileExists(sFuncStr))
							
						}	// if (nDocCounter == nNumOfReportOpen + 1)
						nDocCounter++;
					}

					break;
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)
		} // if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		*ret = 1;
	}	// if (bFound)
	else
	{
		*ret = 0;
	}
}

void runOpenSuiteEx(_user_msg *msg,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret,enumAction action)
{
	CDocTemplate *pTemplate;
	CString sFuncStr;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	CString sLangSet;
	CString sFileToOpen;
	CString sCompareCaption;
	int nDocCounter;
	int nNumOfReportsOpen;
	int nTableIndex;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;

	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;

	// Get language abbrevatein set, from registry; 060111 p�d
	sLangSet = getLangSet();

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		nTableIndex = vecIndex[i].nTableIndex;
		if (nTableIndex == msg->getIndex())
		{
			// Get filename including searchpath to THIS SUITE, as set in
			// the table index vector, for suites and module(s); 051213 p�d
			sVecIndexTableModuleFN = vecIndex[i].szSuite;
			// Need to setup the Actual filename here, because we need to 
			// get the Language set in registry, on Openning a View; 051214 p�d
			sLangFN = vecIndex[i].szLanguageFN;

			sFileToOpen = msg->getFileName();

			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
	
	if (bFound)
	{
		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);

		RLFReader *xml = new RLFReader();
		if (xml->Load(sLangFN))
		{
			sCaption = xml->str(nTableIndex);
		}
		delete xml;

		// Check if the document or module is in this SUITE; 051213 p�d
		if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{

			// First; find out number of Report wiondows already open; 060627 p�d
			POSITION posA = pApp->GetFirstDocTemplatePosition();
			nNumOfReportsOpen = 0;
			while(posA != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(posA);
				pTemplate->GetDocString(sDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				// Need to add a linefeed, infront of the docName.
				// This is because, for some reason, the document title,
				// set in resource, must have a linefeed.
				// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
				sDocName = '\n' + sDocName;

				if (pTemplate && sDocName.Compare(sResStr) == 0)
				{
					POSITION posDOCA = pTemplate->GetFirstDocPosition();
					nDocCounter = 1;

					while (posDOCA != NULL)
					{
						CDocument* pDocumentA = pTemplate->GetNextDoc(posDOCA);
						sCompareCaption = pDocumentA->GetTitle();
						// Compare title of window and count number of matches for
						// a Report window; 060627 p�d
						if (_tcsncmp(sCompareCaption,
												sCaption,
												_tcslen(sCaption)) == 0)
						{
							nNumOfReportsOpen++;
						}	// if (strncmp(sCompareCaption.GetBuffer(),
					}	// while (posDOCA != NULL)
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)

			POSITION pos = pApp->GetFirstDocTemplatePosition();
			while(pos != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(pos);
				pTemplate->GetDocString(sDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				// Need to add a linefeed, infront of the docName.
				// This is because, for some reason, the document title,
				// set in resource, must have a linefeed.
				// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
				sDocName = '\n' + sDocName;

				if (pTemplate && sDocName.Compare(sResStr) == 0)
				{
				
					pTemplate->OpenDocumentFile(NULL);
					// Find the CDocument for this template, and set title.
					// Title is set in Languagefile; OBS! The nTableIndex
					// matches the string id in the languagefile; 051129 p�d
					POSITION posDOC = pTemplate->GetFirstDocPosition();
					while (posDOC != NULL)
					{
						CMDIReportDoc* pDocument = (CMDIReportDoc*)pTemplate->GetNextDoc(posDOC);
						if (nDocCounter == (nNumOfReportsOpen + 1) || nNumOfReportsOpen == 0)
						{

							POSITION posView = pDocument->GetFirstViewPosition();
							if (posView)
							{

								CReportFormView *pReport = (CReportFormView *)pDocument->GetNextView(posView);
								if (pReport)
								{
									pReport->setReportData(sCaption,*msg,action);
									pReport->showReport();

									// set the document title to be the name of the report
									CString sDocTitle;
									if(pReport->m_pReport->ReportOptions->GetName().length() != 0)
									{
										sDocTitle.Format(_T("%s [%s]"), sCaption, pReport->m_pReport->ReportOptions->GetName().Detach());
									}
									else
									{
										// Set the caption of the document. Can be a resource string,
										// a string set in the language xml-file etc.
										if (!sFileToOpen.IsEmpty())
										{
											sDocTitle.Format(_T("%s - [%s]"), sCaption, extractFileName(sFileToOpen));
										}
										else
										{
											sDocTitle.Format(_T("%s - []"),sCaption);
										}
									}

									pDocument->SetTitle(sDocTitle);

								}	// if (pReport)
							}	// if (posView)
						}	// if (nDocCounter == (nNumOfReportsOpen + 1) || nNumOfReportsOpen == 0)
						nDocCounter++;

					}	// while (posDOC != NULL)

					break;
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)
		} // if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		*ret = 1;
	}	// if (bFound)
	else
	{
		*ret = 0;
	}
}


// Use this function, when calling from inside another Suite/User module; 060619 p�d
void DLL_BUILD OpenSuiteEx(_user_msg *msg,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	runOpenSuiteEx(msg,wnd,vecIndex,ret,TEXT_FILE);
}


// Use this function, when calling from inside another Suite/User module
// when opening a Report that'll connect to a Database; 070411 p�d
void DLL_BUILD OpenSuiteEx2(_user_msg *msg,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	runOpenSuiteEx(msg,wnd,vecIndex,ret,DB_FILE);
}
