#ifndef _REPORTFRAMEDOC_H_
#define _REPORTFRAMEDOC_H_

#include "Resource.h"

class CExportItems
{
//private:
	int m_nIndex;
	CString m_sItemName;
public:
	CExportItems(void)
	{
		m_nIndex			= -1;
		m_sItemName		= _T("");
	}
	CExportItems(int idx,LPCTSTR name)
	{
		m_nIndex			= idx;
		m_sItemName		= (name);
	}

	CExportItems(const CExportItems &c)
	{
		*this = c;
	}

	int getIndex(void)				{ return m_nIndex; }
	CString getItemName(void)	{ return m_sItemName; }
};

// Combobox used in toolbar
class CMyToolBarCombo : public CXTPControlComboBox
{
	DECLARE_XTP_CONTROL(CMyToolBarCombo)

	std::vector<CExportItems> m_listExportID;
protected:
	virtual BOOL OnSetPopup(BOOL bPopup);
	virtual void OnSelChanged(void);
};

class CMDIReportDoc : public CDocument
{
protected: // create from serialization only
	CMDIReportDoc();
	DECLARE_DYNCREATE(CMDIReportDoc)

// Attributes
public:

// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIReportDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIReportDoc();
	
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIReportDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CMDIReportFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIReportFrame)

	CXTPToolBar m_wndToolBar;
	CXTPStatusBar m_wndStatusBar;
public:
	CMDIReportFrame();
	virtual ~CMDIReportFrame();

// Attributes
public:
	CXTPDockingPaneManager m_paneManager;

	CXTPPropertyGrid m_wndPropertyGrid;

// Operations
public:
	static XTPDockingPanePaintTheme m_themeCurrent;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIReportFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL
	
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	BOOL bFirstOpened;
	HICON m_hIcon;

	CTBBtn m_TBtn;
	CString m_sPage;
	CString m_sClose;
	CString m_sCancel;
	CString m_sTBtnCaption;

	BOOL m_bOkToClose;
	BOOL m_bFirstOpen;


// Generated message map functions
protected:
	//{{AFX_MSG(CMDIReportFrame)
	afx_msg void OnDestroy(void);
	afx_msg void OnPaint(void);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg	void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);

	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();

	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg LRESULT OnMessageFromReport( WPARAM wParam, LPARAM lParam );

	afx_msg void OnTBtnCancelClick();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



#endif